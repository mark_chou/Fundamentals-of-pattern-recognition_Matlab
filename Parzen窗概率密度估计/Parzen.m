clc;
clear all;
x=randn(1,10000);
px=normpdf(x,0,1);
figure

subplot(4,5,3)
plot(x,px,'.');
title({'Parzen','ԭʼʷֲ'})

a=6;
for h1=[0.25 1 4]
    for N=[1 16 256 1024 4096]
       [tmp]=F_Parzen(x, N, h1);
       subplot(4,5,a)
       plot(x,tmp,'.');
       title(['N=' num2str(N) ',h1=' num2str(h1) ]); 
       a=a+1;
    end
end