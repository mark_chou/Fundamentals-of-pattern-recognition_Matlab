function  [pNx]=F_Parzen(x, N, h1);
pNx=zeros(1,length(x));
hN=h1/sqrt(N);
for j=1:length(x)
    for i=1:N
        pNx(j)=pNx(j)+exp(((x(j)-x(i))/hN).^2/-2)/sqrt(2*pi);
    end
    pNx(j)=pNx(j)/hN/N;
end
end
