# 模式识别基础_Matlab实验

#### 介绍
模式识别基础_Matlab上机实验代码

#### 实验内容
1. Bayes 分类器设计
2. Parzen 窗概率密度估计
3. Fisher 线性分类器
4. KNN 近邻法分类器

