function [ label_test ] = KNN( k,trainData,label_train,testData )
dist=l2_distance(trainData,testData);
[sorted_dist,nearest]=sort(dist);
nearest=nearest(1:k,:);
label_test=label_train(nearest);
end