clc 
clear all

mu1=2;sigma1=2;
mu2=-2;sigma2=4;
x=mu1+sqrt(sigma1)*randn(2,100);
y=mu2+sqrt(sigma2)*randn(2,100);

figure
scatter(x(1,:),x(2,:),'ro');
hold on
scatter(y(1,:),y(2,:),'b*');
axis([-8 8 -8 8]);
legend('w1','w2');
title('两类样本的分类情况');

train_d=[x,y]';
train_label=[ones(1,100),2*ones(1,100)]';
test_d=[[-0.7303,2.1624];[1.4445,-0.1649];[-1.2587,0.9187];[1.2617,-0.2086];[0.7302,1.6587]];

figure;
for k=[1 3 5 7]
    test_label = KNN(k, train_d', train_label', test_d');
    if k==1
        maxCount=test_label;
    else
        [maxCount,idx] = max(test_label); 
    end
    testResults = maxCount; % 得到在K个近邻中出现最多次的类别
    RESULTS((k+1)/2,:) = testResults;
    subplot(2,2,(k+1)/2);
    t1=test_d(testResults==1,:);
    t2=test_d(testResults==2,:);
    scatter(t1(:,1),t1(:,2),'p');
    hold on
    scatter(t2(:,1),t2(:,2),'r');
    axis([-8 8 -8 8]);
    title(['k=',num2str(k)]);
    legend('w1','w2');
end
