clear all
%输入数据
xx=[-3.9847 -3.5549 -1.2401 -0.9780 -0.7932 -2.8531 -2.7605 -3.7287 -3.5414 -2.2692 -3.4549 -3.0752 -3.9934 2.8792 -0.9780 0.7932 1.1882 3.0682 -1.5799 -1.4885 -0.7431 -0.4221 -1.1186 4.2532];
x=xx(:); %转置xx矩阵

m=numel(x);%列出元素个数
resultE=zeros(1,m);%创建新零矩阵共m项

Pw1=0.9;%第一类先验概率
Pw2=0.1;%第二类先验概率
mu1=-1;
sigma1=0.5;%第一类类条件概率密度参数
mu2=2;
sigma2=2;%第二类类条件概率密度参数

%第一类、第二类后验概率计算
[Pwx1,Pwx2]=Bayes_minE(x,Pw1,Pw2,mu1,sigma1,mu2,sigma2);
mm=[Pwx1,Pwx2];
%分别将x中的所有值带入贝叶斯公式计算，求得每个值的后验概率

%得到结果
norm_ind=find(Pwx1>Pwx2);%正常细胞：一类后验概率大于二类
unnorm_ind=find(Pwx1<Pwx2);%正常细胞：一类后验概率小于二类
resultE(norm_ind)=1;%正常细胞于resultE中标记1
resultE(unnorm_ind)=2;%异常细胞标记2
save resultE resultE%保存此矩阵

a= [-5:0.05:5];%准备设置x轴取值范围
[Pwx1_a,Pwx2_a]=Bayes_minE(a,Pw1,Pw2,mu1,sigma1,mu2,sigma2);
%计算a条件下的两条后验概率曲线的对应值

%将x=a范围内的两条后验概率分布曲线画出
subplot(4,1,1)
plot(a,Pwx1_a,'k');
hold on
plot(a,Pwx2_a,'r');
hold on

%将x所求的细胞分类输出到曲线上
plot(x(norm_ind),Pwx1(norm_ind),'b*')
hold on
plot(x(unnorm_ind),Pwx2(unnorm_ind),'ro')
legend('正常细胞后验概率曲线','异常细胞后验概率曲线','正常细胞','异常细胞')%图例
xlabel('样本细胞的观察值')%x轴
ylabel('后验概率')%y轴
title('后验概率分布曲线')%图表标题