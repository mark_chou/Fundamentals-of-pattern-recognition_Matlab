function[Pwx1,Pwx2]=Bayes_minE(x,Pw1,Pw2,mu1,sigma1,mu2,sigma2);

pxw1=normpdf(x,mu1,sigma1); %��̬�ֲ�
pxw2=normpdf(x,mu2,sigma2);

px=pxw1*Pw1+pxw2*Pw2;
Pwx1=pxw1*Pw1./px;
Pwx2=pxw2*Pw2./px;
end
