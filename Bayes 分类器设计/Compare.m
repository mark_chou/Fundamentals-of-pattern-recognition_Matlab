load resultE
load resultR
load resultR0
xx=[-3.9847 -3.5549 -1.2401 -0.9780 -0.7932 -2.8531 -2.7605 -3.7287 -3.5414 -2.2692 -3.4549 -3.0752 -3.9934 2.8792 -0.9780 0.7932 1.1882 3.0682 -1.5799 -1.4885 -0.7431 -0.4221 -1.1186 4.2532];
x=xx(:); %转置xx矩阵
m=numel(x);%列出元素个数
subplot(4,1,4)
ind=1:m;
rr=[resultE;resultR;resultR0]';
bar(ind,rr);

legend('最小错误贝叶斯决策','最小风险贝叶斯决策','0-1最小风险贝叶斯决策')%图例
xlabel('样本细胞序号')%x轴
ylabel('决策结果 1-正常 2-异常')%y轴
title('决策结果对比')%图表标题