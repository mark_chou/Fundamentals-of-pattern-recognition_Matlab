function[Ra1,Ra2]=Bayes_minR(x,Pw1,Pw2,mu1,sigma1,mu2,sigma2);

pxw1=normpdf(x,mu1,sigma1); %��̬�ֲ�
pxw2=normpdf(x,mu2,sigma2);
lambda11=0;
lambda12=6;
lambda21=2;
lambda22=0;

px=pxw1*Pw1+pxw2*Pw2;
Pwx1=pxw1*Pw1./px;
Pwx2=pxw2*Pw2./px;
Ra1=lambda12*Pwx2;
Ra2=lambda21*Pwx1;
end
